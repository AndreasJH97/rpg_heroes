# RPG Heroes

Rpg Heroes is a program that can create characters, weapons, and armor. Each character can equip different types
of weapons and armor. Each character has levelingattributes that increases in different rates. The armor and weapons 
have different attributes that will alter the power of the character if equipped. 


## Usage

Use the test-directory to test the application. 

##Functionality
-Create heroes
-Create weapons
-Create armor
-Level up a hero and increase the different levelingattributes to the specific hero by the correct amount
-Equip character with armor and/or weapon
	-Exeptionhandeling if weapon or armor is below requiredlevel and or invalid type
-Get the total attributes for a hero
-Get the calculated damage for a hero based on equiped items. 
-Display the stats for a hero
