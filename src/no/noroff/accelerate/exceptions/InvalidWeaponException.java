package no.noroff.accelerate.exceptions;

public class InvalidWeaponException extends Throwable {
    public InvalidWeaponException(String errorMsg) {
        super(errorMsg);
    }

}
