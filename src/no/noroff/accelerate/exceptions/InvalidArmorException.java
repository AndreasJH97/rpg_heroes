package no.noroff.accelerate.exceptions;

public class InvalidArmorException extends Throwable {
    public InvalidArmorException(String errorMsg) {
        super(errorMsg);
    }
}
