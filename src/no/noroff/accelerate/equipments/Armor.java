package no.noroff.accelerate.equipments;

import no.noroff.accelerate.hero.utils.HeroAttributes;

public class Armor extends Item {
    private ArmorType armor;
    private HeroAttributes armorAttribute;

    public enum ArmorType{
        CLOTH,
        LEATHER,
        MAIL,
        PLATE

    }

    public Armor(String name,ArmorType armorType, int requiredLevel, Slot slot, HeroAttributes armorAttribute) {
        this.name=name;
        this.armor=armorType;
        this.requiredLevel=requiredLevel;
        this.slot=slot;
        this.armorAttribute=armorAttribute;
    }

    public ArmorType getArmorType() {
        return armor;
    }

    public HeroAttributes getArmorAttribute() {
        return armorAttribute;
    }

    @Override
    public String toString() {
        return "no.noroff.accelerate.weapon.Armor{" +
                "armor=" + armor +
                ", armorAttribute=" + armorAttribute +
                ", name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slot=" + slot +
                '}';
    }
}
