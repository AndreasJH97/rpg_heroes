package no.noroff.accelerate.equipments;

public class Weapon extends Item {
    public enum WeaponType {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND;
    }
    public int weaponDamage;
    private WeaponType weapon;

    public Weapon(String name,int requiredLevel,WeaponType weaponType, int weaponDamage) {
        this.name=name;
        this.requiredLevel=requiredLevel;
        slot=Slot.WEAPON;
        weapon=weaponType;
        this.weaponDamage = weaponDamage;
    }

    public WeaponType getWeaponType() {
        return weapon;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }

    @Override
    public String toString() {
        return "no.noroff.accelerate.weapon.Weapon{" +
                " weaponType=" + weapon +
                ", weaponDamage=" + weaponDamage +
                ", name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slot=" + slot +
                '}';
    }
}
