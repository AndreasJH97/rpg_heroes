package no.noroff.accelerate.equipments;

public abstract class Item {
    protected String name;
    protected int requiredLevel;
    protected Slot slot;
    public enum Slot{
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }

}
