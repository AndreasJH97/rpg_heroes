package no.noroff.accelerate.hero;

import no.noroff.accelerate.hero.utils.HeroAttributes;
import no.noroff.accelerate.equipments.Item;
import no.noroff.accelerate.equipments.Armor;
import no.noroff.accelerate.equipments.Weapon;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;

import java.util.Arrays;
import java.util.HashMap;

public abstract class Hero {
    private String name;
    private int lvl;
    private HeroAttributes levelAttributes;
    private HashMap<Item.Slot, Item> equipment;
    private String[] validWeaponType;
    private String[] validArmorType;

    public Hero(String name) {
        this.name = name;
        setLvl(1);
        setEquipment();
    }

    public void levelUp(Hero hero){ //Method for increasing level and levelattributes
        hero.lvl++; //Incrementing level with 1 for each time a hero levels up
        IncreaseLevelAttributes(hero, hero.getLevelAttributes());
    }
    //IncreaseLevelAttribute is overwritten and calling super() with the correct levelingAttributes
    public void IncreaseLevelAttributes(Hero hero, HeroAttributes attributes){
        //Adding existing leveling attributes with new leveling attributes and setting the new values to the hero
        setLevelAttributes(
                new HeroAttributes(((hero.getLevelAttributes().getStrength())+(attributes.getStrength())),
                        ((hero.getLevelAttributes().getDexterity())+(attributes.getDexterity())),
                        ((hero.getLevelAttributes().getIntelligence())+(attributes.getIntelligence()))));
    }
    //Method for equipping weapons
    public void equipWeapon(Hero hero, Item item) throws InvalidWeaponException {
        Weapon w=(Weapon) item;
        //Checking if the weapon is valid for the chosen character ->Requiredlevel and WeponType
        if(Arrays.toString(hero.getValidWeaponType()).contains(w.getWeaponType().name()) &&
                hero.getLvl()>=w.getRequiredLevel()){
            hero.getEquipment().put(w.getSlot(),w);//Equipping valid weapon
        }else{
            //Weapon is not valid for selected Character
            throw new InvalidWeaponException("Your character is not able to equip "+ w.getName());
        }

    }
    //Method for equipping armor
    public void equipArmor(Hero hero, Item item) throws InvalidArmorException {
        Armor a=(Armor) item;
        //Checking if the armor is valid for the chosen character ->Requiredlevel and ArmorType
        if(Arrays.toString(hero.getValidArmorType()).contains(a.getArmorType().name())&&
                hero.getLvl()>=a.getRequiredLevel()){
            hero.getEquipment().put(a.getSlot(),a); //Equipping valid armor
        }else{
            //Armor is not valid for selected Character
            throw new InvalidArmorException("Your character is not able to equip "+ a.getName());
        }
    }
    //Calculating a heroes damage, Overwritten and called by subClasses to get correct DamagingAttribute to calculate with
    //DamagingAttribute is based on what character you are playing
    public double calculateDamage(Hero hero, int totalDamagingAttribute) {
        Weapon weapon=(Weapon)(hero.getEquipment().get(Item.Slot.WEAPON));
        int wDamage= weapon!=null? weapon.getWeaponDamage():1;
        //Damage is calculated with this formula WeaponDamage * (1 + DamagingAttribute/100)
        double damage=((wDamage) * ((1) + (double) (totalDamagingAttribute) / (100)));
        return damage;
        }

    //Method that's adding all the different heroattributes from all equipment and levelattributes
    public HeroAttributes getTotalAttributes(Hero hero){
        int strength =hero.getLevelAttributes().getStrength();
        int dexterity=hero.getLevelAttributes().getDexterity();
        int intelligence=hero.getLevelAttributes().getIntelligence();

        //Loop for summing all the Armour attributes
        for (Item armor : equipment.values()) { //Looping through all the slots
            if (armor!=null && armor.getClass().getSimpleName().equals("Armor")){ //Check if armour is equipped
                Armor a=(Armor) armor;
                strength+=(a.getArmorAttribute().getStrength());
                dexterity+=(a.getArmorAttribute().getDexterity());
                intelligence+=(a.getArmorAttribute().getIntelligence());

            }
        }
        return new HeroAttributes(strength,dexterity,intelligence);
    }
    //Method to display a heroes stats
    public String display(Hero hero) {
        StringBuilder builder= new StringBuilder();
        builder.append("Name: " +hero.getName()+"\n");
        builder.append("Class: " +hero.getClass().getSimpleName()+"\n");
        builder.append("Level: " +hero.getLvl()+"\n");
        builder.append("Total Attributes {strength="+hero.getTotalAttributes(hero).getStrength()+
                        ", dexterity=" +hero.getTotalAttributes(hero).getDexterity()
                +", intelligence=" + hero.getTotalAttributes(hero).getIntelligence()+"}\n");
        builder.append("Calculated damage: "+hero.calculateDamage(hero,1));
        return builder.toString();
    }


    public String getName() {
        return name;
    }


    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public HeroAttributes getLevelAttributes() {
        return levelAttributes;
    }

    public void setLevelAttributes(HeroAttributes levelAttributes) {
        this.levelAttributes = levelAttributes;
    }

    public HashMap<Item.Slot, Item> getEquipment() {
        return equipment;
    }

    public void setEquipment() {
        equipment=new HashMap<>();
        equipment.put(Item.Slot.WEAPON,null);
        equipment.put(Item.Slot.HEAD,null);
        equipment.put(Item.Slot.BODY,null);
        equipment.put(Item.Slot.LEGS,null);
    }
    public String[] getValidWeaponType() {
        return validWeaponType;
    }

    public void setValidWeaponType(String[] validWeaponType) {
        this.validWeaponType = validWeaponType;
    }
    public String[] getValidArmorType() {
        return validArmorType;
    }
    public void setValidArmorType(String[] validArmorType) {
        this.validArmorType = validArmorType;
    }
}
