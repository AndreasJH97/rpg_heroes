package no.noroff.accelerate.hero.character;

import no.noroff.accelerate.hero.Hero;
import no.noroff.accelerate.hero.utils.HeroAttributes;

public class Ranger extends Hero {


    public Ranger(String name) {
        super(name);
        setLevelAttributes(new HeroAttributes(1, 7, 1));
        setValidWeaponType(new String[]{"Bow"});
        setValidArmorType(new String[]{"Leather","Mail"});
    }

    @Override //Calling super() with the correct damageAttribute based on the chosen class
    public double calculateDamage(Hero hero, int damagingAttribute) {
        //sending the characters damagingAttribute to be calculated with
        return super.calculateDamage(hero, hero.getLevelAttributes().getDexterity());
    }
    @Override //Calling super() with the correct leveling attributes based on the chosen class
    public void IncreaseLevelAttributes(Hero hero, HeroAttributes attributes) {
        super.IncreaseLevelAttributes(hero, new HeroAttributes(1,5,1));
    }
}
