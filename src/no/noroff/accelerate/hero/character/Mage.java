package no.noroff.accelerate.hero.character;

import no.noroff.accelerate.hero.Hero;
import no.noroff.accelerate.hero.utils.HeroAttributes;

public class Mage extends Hero {

    public Mage(String name) {
        super(name);
        setLevelAttributes(new HeroAttributes(1, 1, 8));
        setValidWeaponType(new String[]{"STAFF", "WAND"});
        setValidArmorType(new String[]{"CLOTH"});
    }

    @Override //Calling super() with the correct damageAttribute based on the chosen class
    public double calculateDamage(Hero hero, int damagingAttribute) {
        int totalIntelligence= hero.getTotalAttributes(hero).getIntelligence();
        //sending the characters damagingAttribute to be calculated with
        return super.calculateDamage(hero, totalIntelligence);
    }

    @Override //Calling super() with the correct leveling attributes based on the chosen class
    public void IncreaseLevelAttributes(Hero hero, HeroAttributes attributes) {
        super.IncreaseLevelAttributes(hero, new HeroAttributes(1,1,5));
    }
}
