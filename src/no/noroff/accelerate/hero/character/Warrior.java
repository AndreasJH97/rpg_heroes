package no.noroff.accelerate.hero.character;

import no.noroff.accelerate.hero.Hero;
import no.noroff.accelerate.hero.utils.HeroAttributes;

public class Warrior extends Hero {


    public Warrior(String name) {
        super(name);
        setLevelAttributes(new HeroAttributes(5, 2, 1));
        setValidWeaponType(new String[]{"Axe", "Hammer","Sword"});
        setValidArmorType(new String[]{"Mail","Plate"});
    }

    @Override //Calling super() with the correct damageAttribute based on the chosen class
    public double calculateDamage(Hero hero, int damagingAttribute) {
        //sending the characters damagingAttribute to be calculated with
        return super.calculateDamage(hero, hero.getLevelAttributes().getStrength());
    }
    @Override //Calling super() with the correct leveling attributes based on the chosen class
    public void IncreaseLevelAttributes(Hero hero, HeroAttributes attributes) {
        super.IncreaseLevelAttributes(hero, new HeroAttributes(3,2,1));
    }
}
