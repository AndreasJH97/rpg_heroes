package no.noroff.accelerate;

import no.noroff.accelerate.equipments.Item;
import no.noroff.accelerate.hero.character.Mage;
import no.noroff.accelerate.hero.character.Ranger;
import no.noroff.accelerate.hero.character.Rogue;
import no.noroff.accelerate.hero.character.Warrior;
import no.noroff.accelerate.equipments.Armor;
import no.noroff.accelerate.equipments.Weapon;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.hero.Hero;
import no.noroff.accelerate.hero.utils.HeroAttributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    Hero mage;
    Hero warrior;
    Hero rouge;
    Hero ranger;
    @BeforeEach
    void setUp(){
        mage=new Mage("Andreas");
        warrior=new Warrior("Andreas");
        rouge=new Rogue("Andreas");
        ranger=new Ranger("Andreas");
    }
    @Test
    public void mage_validName_ShouldReturnValidName(){
        //Arrange
        String expected="Andreas";
        //Act
        String actual= mage.getName();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void mage_validLevel_ShouldReturnValidLevel(){
        //Arrange
        int expected=1;
        //Act
        int actual= mage.getLvl();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void mage_validLevelAttributes_ShouldReturnValidLevelAttributes(){
        //Arrange
        HeroAttributes expected=new HeroAttributes(1,1,8);
        //Act
        HeroAttributes actual= mage.getLevelAttributes();
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mage_validHashMap_ShouldReturnValidHashMap(){
        //Arrange
        HashMap<Item.Slot,Item> expected=new HashMap<>();
        expected.put(Item.Slot.WEAPON,null);
        expected.put(Item.Slot.HEAD,null);
        expected.put(Item.Slot.BODY,null);
        expected.put(Item.Slot.LEGS,null);
        //Act
        HashMap<Item.Slot,Item> actual= mage.getEquipment();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void mage_validWeaponType_ShouldReturnValidWeaponType(){
        //Arrange
        String expected="[STAFF, WAND]";
        //Act
        String actual= Arrays.toString(mage.getValidWeaponType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mage_validArmorType_ShouldReturnValidArmorType(){
        //Arrange
        String expected="[CLOTH]";
        //Act
        String actual= Arrays.toString(mage.getValidArmorType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mage_IncreaseLevelAttributes_ShouldIncreaseCharactersLevelAttributes(){
        //Arrange
        HeroAttributes expected= new HeroAttributes(2,2,13);
        //Act
        mage.levelUp(mage);
        //Assert
        assertEquals(mage.getLevelAttributes(), expected);
    }

    @Test
    public void mageNoWeapon_getCalculateDamage_ShouldReturnCalculatedDamage(){
        //Arrange
        double expected= 1.08;
        //Act
        double actual= mage.calculateDamage(mage, 1);
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mageValidWeapon_getCalculateDamage_ShouldReturnCalculatedDamage(){
        //Arrange
        Weapon weapon=new Weapon("Magic Wand",1, Weapon.WeaponType.WAND,25);
        try {
            mage.equipWeapon(mage,weapon);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }
        double expected= 27;
        //Act
        double actual= mage.calculateDamage(mage, 1);
        //Assert
        assertEquals(actual, expected);
    }

    @Test
    public void mageReplacedWeapon_getCalculateDamage_ShouldReturnCalculatedDamage(){
        //Arrange
        Weapon weapon=new Weapon("Magic Wand",1, Weapon.WeaponType.WAND,10);
        Weapon weapon1=new Weapon("Magic Wand",1, Weapon.WeaponType.WAND,20);
        try {
            mage.equipWeapon(mage,weapon);
            mage.equipWeapon(mage,weapon1);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }
        double expected= 21.6;
        //Act
        double actual= mage.calculateDamage(mage, 1);
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mageValidWeaponAndArmor_getCalculateDamage_ShouldReturnCalculatedDamage(){
        //Arrange
        Weapon weapon=new Weapon("Magic Wand",1, Weapon.WeaponType.WAND,41);
        Armor armor= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,1, Item.Slot.LEGS,new HeroAttributes(2,3,4));
        try {
            mage.equipWeapon(mage,weapon);
            mage.equipArmor(mage,armor);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        double expected= 45.92;
        //Act
        double actual= mage.calculateDamage(mage, 1);
        //Assert
        assertEquals(actual, expected);
    }



    //----------------------------------------Warrior-----------------------------------------------
    @Test
    public void warrior_validName_ShouldReturnValidName(){
        //Arrange
        String expected="Andreas";
        //Act
        String actual= warrior.getName();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void warrior_validLevel_ShouldReturnValidLevel(){
        //Arrange
        int expected=1;
        //Act
        int actual= warrior.getLvl();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void warrior_validLevelAttributes_ShouldReturnValidLevelAttributes(){
        //Arrange
        HeroAttributes expected=new HeroAttributes(5,2,1);
        //Act
        HeroAttributes actual= warrior.getLevelAttributes();
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void warrior_validHashMap_ShouldReturnValidHashMap(){
        //Arrange
        HashMap<Item.Slot,Item> expected=new HashMap<>();
        expected.put(Item.Slot.WEAPON,null);
        expected.put(Item.Slot.HEAD,null);
        expected.put(Item.Slot.BODY,null);
        expected.put(Item.Slot.LEGS,null);
        //Act
        HashMap<Item.Slot,Item> actual= warrior.getEquipment();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void warrior_validWeaponType_ShouldReturnValidWeaponType(){
        //Arrange
        String expected="[Axe, Hammer, Sword]";
        //Act
        String actual= Arrays.toString(warrior.getValidWeaponType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void warrior_validArmorType_ShouldReturnValidArmorType(){
        //Arrange
        String expected="[Mail, Plate]";
        //Act
        String actual= Arrays.toString(warrior.getValidArmorType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void warrior_IncreaseLevelAttributes_ShouldIncreaseCharactersLevel(){
        //Arrange
        HeroAttributes expected= new HeroAttributes(8,4,2);
        //Act
        warrior.levelUp(warrior);
        //Assert
        assertEquals(warrior.getLevelAttributes(), expected);
    }


    //-------------------------------Rouge----------------------------------------------------------

    @Test
    public void rouge_validName_ShouldReturnValidName(){
        //Arrange
        String expected="Andreas";
        //Act
        String actual= rouge.getName();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void rouge_validLevel_ShouldReturnValidLevel(){
        //Arrange
        int expected=1;
        //Act
        int actual= rouge.getLvl();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void rouge_validLevelAttributes_ShouldReturnValidLevelAttributes(){
        //Arrange
        HeroAttributes expected=new HeroAttributes(2,6,1);
        //Act
        HeroAttributes actual= rouge.getLevelAttributes();
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void rouge_validHashMap_ShouldReturnValidHashMap(){
        //Arrange
        HashMap<Item.Slot,Item> expected=new HashMap<>();
        expected.put(Item.Slot.WEAPON,null);
        expected.put(Item.Slot.HEAD,null);
        expected.put(Item.Slot.BODY,null);
        expected.put(Item.Slot.LEGS,null);
        //Act
        HashMap<Item.Slot,Item> actual= rouge.getEquipment();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void rouge_validWeaponType_ShouldReturnValidWeaponType(){
        //Arrange
        String expected="[Dagger, Sword]";
        //Act
        String actual= Arrays.toString(rouge.getValidWeaponType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void rouge_validArmorType_ShouldReturnValidArmorType(){
        //Arrange
        String expected="[Leather, Mail]";
        //Act
        String actual= Arrays.toString(rouge.getValidArmorType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void rouge_IncreaseLevelAttributes_ShouldIncreaseCharactersLevel(){
        //Arrange
        HeroAttributes expected= new HeroAttributes(3,10,2);
        //Act
        rouge.levelUp(rouge);
        //Assert
        assertEquals(rouge.getLevelAttributes(), expected);
    }

    //-------------------------------------Ranger-------------------------------------------------

    @Test
    public void ranger_validName_ShouldReturnValidName(){
        //Arrange
        String expected="Andreas";
        //Act
        String actual= ranger.getName();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void ranger_validLevel_ShouldReturnValidLevel(){
        //Arrange
        int expected=1;
        //Act
        int actual= ranger.getLvl();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    public void ranger_validLevelAttributes_ShouldReturnValidLevelAttributes(){
        //Arrange
        HeroAttributes expected=new HeroAttributes(1,7,1);
        //Act
        HeroAttributes actual= ranger.getLevelAttributes();
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void ranger_validHashMap_ShouldReturnValidHashMap(){
        //Arrange
        HashMap<Item.Slot,Item> expected=new HashMap<>();
        expected.put(Item.Slot.WEAPON,null);
        expected.put(Item.Slot.HEAD,null);
        expected.put(Item.Slot.BODY,null);
        expected.put(Item.Slot.LEGS,null);
        //Act
        HashMap<Item.Slot,Item> actual= ranger.getEquipment();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void ranger_validWeaponType_ShouldReturnValidWeaponType(){
        //Arrange
        String expected="[Bow]";
        //Act
        String actual= Arrays.toString(ranger.getValidWeaponType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void ranger_validArmorType_ShouldReturnValidArmorType(){
        //Arrange
        String expected="[Leather, Mail]";
        //Act
        String actual= Arrays.toString(ranger.getValidArmorType());
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void ranger_IncreaseLevelAttributes_ShouldIncreaseCharactersLevel(){
        //Arrange
        HeroAttributes expected= new HeroAttributes(2,12,2);
        //Act
        ranger.levelUp(ranger);
        //Assert
        assertEquals(ranger.getLevelAttributes(), expected);
    }

    //--------------------------------General Methods-------------------------------------------------
    @Test
    public void hero_LevelUp_ShouldIncreaseCharactersLevel(){
        //Arrange
        int expected=2;
        //Act
        mage.levelUp(mage);
        //Assert
        assertEquals(mage.getLvl(), expected);
    }
    @Test
    public void mageNoArmor_getTotalAttributes_ShouldReceiveTotalAttributes(){
        //Arrange
        HeroAttributes expected= new HeroAttributes(1,1,8);
        //Act
        HeroAttributes actual= mage.getTotalAttributes(mage);
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mageOneArmor_getTotalAttributes_ShouldReceiveTotalAttributes(){
        //Arrange
        Armor armor= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,1, Item.Slot.HEAD,new HeroAttributes(2,3,4));
        try {
            mage.equipArmor(mage,armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        HeroAttributes expected= new HeroAttributes(3,4,12);
        //Act
        HeroAttributes actual= mage.getTotalAttributes(mage);
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mageTwoArmor_getTotalAttributes_ShouldReceiveTotalAttributes(){
        //Arrange
        Armor armor= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,1, Item.Slot.HEAD,new HeroAttributes(2,3,4));
        Armor armor1= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,1, Item.Slot.BODY,new HeroAttributes(4,3,1));
        try {
            mage.equipArmor(mage,armor);
            mage.equipArmor(mage,armor1);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        HeroAttributes expected= new HeroAttributes(7,7,13);
        //Act
        HeroAttributes actual= mage.getTotalAttributes(mage);
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mageReplacedArmor_getTotalAttributes_ShouldReceiveTotalAttributes(){
        //Arrange
        Armor armor= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,1, Item.Slot.LEGS,new HeroAttributes(2,3,4));
        Armor armor1= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,1, Item.Slot.LEGS,new HeroAttributes(5,4,3));
        try {
            mage.equipArmor(mage,armor);
            mage.equipArmor(mage,armor1);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        HeroAttributes expected= new HeroAttributes(6,5,11);
        //Act
        HeroAttributes actual= mage.getTotalAttributes(mage);
        //Assert
        assertEquals(actual, expected);
    }
    @Test
    public void mage_displayStats_ShouldDisplayStats(){
        //Arrange
        String expected= "Name: Andreas\nClass: Mage\nLevel: 1\nTotal Attributes {strength=1"+
                ", dexterity=1, intelligence=8}\nCalculated damage: 1.08";
        //Act
        String actual= mage.display(mage);
        //Assert
        assertEquals(actual, expected);
    }

}