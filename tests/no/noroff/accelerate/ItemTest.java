package no.noroff.accelerate;

import no.noroff.accelerate.equipments.Item;
import no.noroff.accelerate.hero.character.Mage;
import no.noroff.accelerate.equipments.Armor;
import no.noroff.accelerate.equipments.Weapon;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.hero.Hero;
import no.noroff.accelerate.hero.utils.HeroAttributes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    Item armor;
    Item weapon;

    @BeforeEach
    void setUp() {
        armor= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,1, Item.Slot.HEAD,new HeroAttributes(2,3,4));
        weapon=new Weapon("Magic Wand",1, Weapon.WeaponType.WAND,50);
    }

    //----------------------------------Armor-------------------------------------------------------------------
    @Test
    void armor_getName_shouldReturnCorrectName() {
        //Arrange
        String expected="Thick Cloth";
        //Act
        String actual= armor.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void armor_getRequiredLevel_shouldReturnCorrectReqLevel() {
        //Arrange
        int expected=1;
        //Act
        int actual= armor.getRequiredLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void armor_getSlot_shouldReturnCorrectSlot() {
        //Arrange
        Item.Slot expected= Item.Slot.HEAD;
        //Act
        Item.Slot actual= armor.getSlot();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void armor_getArmorType_shouldReturnCorrectArmorType() {
        //Arrange
        Armor.ArmorType expected= Armor.ArmorType.CLOTH;
        //Act
        Armor.ArmorType actual= ((Armor) armor).getArmorType();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void armor_getArmorAttributes_shouldReturnCorrectArmorAttributes() {
        //Arrange
        HeroAttributes expected= new HeroAttributes(2,3,4);
        //Act
        HeroAttributes actual= ((Armor) armor).getArmorAttribute();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void equip_equipInvalidReqLevelArmor_shouldReturnInvalidArmorException() {
        //Arrange
        Armor invalidArmor= new Armor("Thick Cloth", Armor.ArmorType.CLOTH,4, Item.Slot.HEAD,new HeroAttributes(2,3,4));
        Hero mage=new Mage("Andreas");
        String expected= "Your character is not able to equip Thick Cloth";
        //Act
        String actual = null;
        try{mage.equipArmor(mage, invalidArmor);
        }catch(InvalidArmorException e){
            actual=e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void equip_equipInvalidTypeArmor_shouldReturnInvalidArmorException() {
        //Arrange
        Armor invalidArmor= new Armor("Heavy PLate", Armor.ArmorType.PLATE,1, Item.Slot.HEAD,new HeroAttributes(2,3,4));
        Hero mage=new Mage("Andreas");
        String expected= "Your character is not able to equip Heavy PLate";
        //Act
        String actual = null;
        try{mage.equipArmor(mage, invalidArmor);
        }catch(InvalidArmorException e){
            actual=e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void equip_equipValidArmor_shouldEquipHeroWithCorrectArmor() {
        //Arrange
        Hero mage=new Mage("Andreas");
        HashMap<Item.Slot,Item> expected=new HashMap<>();
        expected.put(Item.Slot.WEAPON,null);
        expected.put(Item.Slot.HEAD,armor);
        expected.put(Item.Slot.BODY,null);
        expected.put(Item.Slot.LEGS,null);
        //Act
        try{mage.equipArmor(mage, armor);
        }catch(InvalidArmorException e){
            e.printStackTrace();
        }
        HashMap<Item.Slot,Item> actual = mage.getEquipment();
        //Assert
        assertEquals(expected,actual);
    }


    //----------------------------------Weapon-------------------------------------------------------------------
    @Test
    void weapon_getName_shouldReturnCorrectName() {
        //Arrange
        String expected="Magic Wand";
        //Act
        String actual= weapon.getName();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void weapon_getRequiredLevel_shouldReturnCorrectReqLevel() {
        //Arrange
        int expected=1;
        //Act
        int actual= weapon.getRequiredLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void weapon_getSlot_shouldReturnCorrectSlot() {
        //Arrange
        Item.Slot expected= Item.Slot.WEAPON;
        //Act
        Item.Slot actual= weapon.getSlot();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void weapon_getWeaponType_shouldReturnCorrectWeaponType() {
        //Arrange
        Weapon.WeaponType expected= Weapon.WeaponType.WAND;
        //Act
        Weapon.WeaponType actual= ((Weapon) weapon).getWeaponType();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void weapon_getWeaponDamage_shouldReturnCorrectArmorAttributes() {
        //Arrange
        int expected= 50;
        //Act
        int actual= ((Weapon) weapon).getWeaponDamage();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void equip_equipInvalidReqLevelWeapon_shouldReturnInvalidWeaponException() {
        //Arrange
        Weapon invalidWeapon=new Weapon("Magic Wand",3, Weapon.WeaponType.WAND,50);
        Hero mage=new Mage("Andreas");
        String expected= "Your character is not able to equip Magic Wand";
        //Act
        String actual = null;
        try{mage.equipWeapon(mage, invalidWeapon);
        }catch(InvalidWeaponException e){
            actual=e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void equip_equipInvalidTypeWeapon_shouldReturnInvalidWeaponException() {
        //Arrange
        Weapon invalidWeapon=new Weapon("Incredible Axe",1, Weapon.WeaponType.AXE,50);
        Hero mage=new Mage("Andreas");
        String expected= "Your character is not able to equip Incredible Axe";
        //Act
        String actual = null;
        try{mage.equipWeapon(mage, invalidWeapon);
        }catch(InvalidWeaponException e){
            actual=e.getMessage();
        }
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void equip_equipValidWeapon_shouldEquipHeroWithWeapon() {
        //Arrange
        Hero mage=new Mage("Andreas");
        HashMap<Item.Slot,Item> expected=new HashMap<>();
        expected.put(Item.Slot.WEAPON,weapon);
        expected.put(Item.Slot.HEAD,null);
        expected.put(Item.Slot.BODY,null);
        expected.put(Item.Slot.LEGS,null);
        //Act
        try{mage.equipWeapon(mage, weapon);
        }catch(InvalidWeaponException e){
            e.printStackTrace();
        }
        HashMap<Item.Slot,Item> actual = mage.getEquipment();
        //Assert
        assertEquals(expected,actual);
    }


}